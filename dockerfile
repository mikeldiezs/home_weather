FROM python:3.8-slim

ENV TZ=Europe/Madrid

RUN mkdir home_weather/

COPY . /home_weather
WORKDIR /home_weather

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN pip install schedule==1.0.0
RUN pip install influxdb-client==1.18.0
RUN pip install requests==2.25.1
RUN pip install colorama==0.4.4

CMD ["python", "-u", "./__main__.py"]