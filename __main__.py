import schedule, time
from utils.tools import print_time
from colorama import Fore
from config.auxconfig import JSON_MEASURES_KEYS, DEVICES_ID, COLON_MAPPER

from interfaces.api import ApiConnection
from data.processing import DataExtract
from interfaces.database import Influx

def get_stations_data():
    """
    Job to get and ingest weather stations data.
    """

    # get the data from the api
    response = api.get_station_data(api.access_token)

    if response.status_code != 200:
        print_time(arg=Fore.YELLOW + f"WARNING: It was not possible to get the data from the API due to HTTP error {response.status_code}" + Fore.RESET)
        # refresh the token
        api.refresh_access_token(api.refresh_token)
        # get the data from the api using the refreshed token
        response = api.get_station_data(api.access_token)
        if response.status_code != 200: 
            response = None

    if response is not None:
        # process the data before the ingest
        data = DataExtract()
        line_protocol_data = data.prepare_station_data(response)

        # ingest the data in the database
        if line_protocol_data is not None:
            influx = Influx()
            influx.ingest_influx(line_protocol_data)

    else:
        print_time(arg=Fore.RED + "ERROR: It was not possible to get the data from the API, no data ingested in the database." + Fore.RESET)
    
    return None

def get_measures():
    """
    Job to get and ingest weather stations data.
    """

    line_protocol_data = []

    for device in JSON_MEASURES_KEYS.keys():
        device_id = DEVICES_ID[device]
        module_api_id = device_id.replace(":", COLON_MAPPER)

        for measure, field in JSON_MEASURES_KEYS[device].items():
            # get the data from the api
            response = api.get_measure_data(api.access_token, module_api_id, measure)

            if response.status_code != 200:
                print_time(arg=Fore.YELLOW + f"WARNING: It was not possible to get the data from the API due to HTTP error {response.status_code}" + Fore.RESET)
                # refresh the token
                api.refresh_access_token(api.refresh_token)
                # get the data from the api using the refreshed token
                response = api.get_measure_data(api.access_token, module_api_id, measure)
                if response.status_code != 200:
                    response = None

            if response is not None:
                # process the data before the ingest
                data = DataExtract()
                processed_data = data.get_measure(response, device, device_id, field)

                if processed_data is not None:
                    line_protocol_data.append(processed_data)
            else:
                print_time(arg=Fore.RED + f"ERROR: It was not possible to get the data of measure {field} of sensor {device}." + Fore.RESET)

    # ingest the data in the database
    influx = Influx()
    influx.ingest_influx(line_protocol_data)

    return None

# define the scheduling
schedule.every(5).minutes.at(":00").do(get_measures)
schedule.every(5).minutes.at(":30").do(get_stations_data)

if __name__ == "__main__":

    print_time(arg=Fore.CYAN + "****** Initialization of Home Weather service ******" + Fore.RESET)

    # create the API connection instance
    api = ApiConnection()
    # get the access token for first time
    api.get_access_token()

    while True:
        schedule.run_pending()
        time.sleep(1)