from influxdb_client import InfluxDBClient, WriteOptions
import configparser
from config.auxconfig import CONFIG_FILE
from utils.tools import print_time
from colorama import Fore

class Influx():

    """
    Netatmo API database structure in Influx 2:
    ------------------------------------------
    
    home_weather <---- BUCKET
        |__station
            |__home_id
            |__station_id [TAG]
            |__module_name
            |__wifi_status
            |__reachable
            |__co2_calibrating
            |__altitude
            |__longitude
            |__latitude
            |__time_utc
            |__temperature
            |__co2
            |__humidity
            |__noise
            |__pressure
            |__absolute_pressure
        |__outdoor
            |__id [TAG]
            |__module_name
            |__battery_percent
            |__reachable
            |__rf_status
            |__battery_vp
            |__time_utc
            |__temperature
            |__humidity
        |__pluviometer
            |__id [TAG]
            |__module_name
            |__battery_percent
            |__reachable
            |__rf_status
            |__battery_vp
            |__time_utc
            |__rain
            |__sum_rain_1
            |__sum_rain_24
        |__anemometer
            |__id [TAG]
            |__module_name
            |__battery_percent
            |__reachable
            |__rf_status
            |__battery_vp
            |__time_utc
            |__windspeed
            |__windangle
            |__gustspeed
            |__gustangle
    """

    def __init__(self):

        config = configparser.ConfigParser()
        config.read(CONFIG_FILE)

        # influxdb configuration
        self.influx_url = config['influx2']['url']
        self.influx_org = config['influx2']['org']
        self.influx_token = config['influx2']['token']
        self.influx_bucket = config['influx2']['bucket']

        # starting info
        print_time(arg='Starting InfluxDB client...')

    def ingest_influx(self, data):
            
        # write data into influx
        try:

            with InfluxDBClient(url=self.influx_url, token=self.influx_token, org=self.influx_org) as _client:

                with _client.write_api(write_options=WriteOptions(batch_size=5000,
                                                                flush_interval=10_000,
                                                                jitter_interval=2_000,
                                                                retry_interval=5_000,
                                                                max_retries=5,
                                                                max_retry_delay=30_000,
                                                                exponential_base=2)) as _write_client:

                    _write_client.write(self.influx_bucket, self.influx_org, data)
                    print_time(arg=Fore.GREEN + f"Weather data ingest in bucket {self.influx_bucket} successfully done" + Fore.RESET)
                    return None
        except ConnectionError as c:
            print(c)