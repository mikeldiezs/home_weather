import requests, configparser, time
from datetime import datetime
from utils.tools import print_time
from colorama import Fore
from config.auxconfig import CONFIG_FILE, ACCESS_TOKEN, REFRESH_TOKEN, COLON_MAPPER, DATA_SCALE, TIME_SCALE

class ApiConnection():

    def __init__(self):
        config = configparser.ConfigParser()
        config.read(CONFIG_FILE)

        # docker secrets
            # docker secrets path to get api credentials
        self.secret_path = config['docker_secrets']['path']
            # client_id
        secret_client_id = config['docker_secrets']['secret_client_id']
        self.client_id = self.get_secret(secret_client_id)
            # client_secret
        secret_client_secret = config['docker_secrets']['secret_client_secret']
        self.client_secret = self.get_secret(secret_client_secret)
            # username
        secret_username = config['docker_secrets']['secret_username']
        self.username = self.get_secret(secret_username)
            # password
        secret_password = config['docker_secrets']['secret_password']
        self.password = self.get_secret(secret_password)

        # api auth grant type
        self.grant_type_access = config['netatmo_api']['grant_type_access']
        self.grant_type_refresh = config['netatmo_api']['grant_type_refresh']
        
        # devices id
        self.id_station = config['netatmo_devices']['id_station']
        id_outdoor = config['netatmo_devices']['id_outdoor']
        id_pluviometer = config['netatmo_devices']['id_pluviometer']
        id_anemometer = config['netatmo_devices']['id_anemometer']
        self.devices_list = [self.id_station, id_outdoor, id_pluviometer, id_anemometer]

        # api endpoints
        self.url_token = config['netatmo_api']['url_token']
            # stations
        basic_url_api_stations = config['netatmo_api']['url_api_stations']
        self.url_api_stations = basic_url_api_stations + self.id_station.replace(":", COLON_MAPPER)
            # measures
        self.url_api_devices = config['netatmo_api']['url_api_devices']

        # initialization of variables
        self.access_token = None
        self.refresh_token = None

    def get_secret(self, secret):
        """
        Get a defined secret from Docker secrets. 
        Used on this class initialization.
        """

        try:
            with open(f'{self.secret_path}{secret}', 'r') as secret_file:
                return secret_file.read()
        except IOError:
            print_time(arg=Fore.RED + f"ERROR: It was not possible to get the secret {secret}" + Fore.RESET)
            return None

    def get_access_token(self):
        """
        Make the post to obtain the access token and the refresh token.
        Only runs when the service is up for first time of after a restart.
        The refresh token does not change once it is obtained.
        """

        print_time(arg="Trying to get the access token, please wait.")

        payload = f"grant_type={self.grant_type_access}&client_secret={self.client_secret}&client_id={self.client_id}&" + \
                  f"username={self.username}&password={self.password}"

        headers = {"Content-type": "application/x-www-form-urlencoded"}

        response = requests.post(self.url_token, data=payload, headers=headers)
        
        if response.status_code == 200:
            self.access_token = response.json()[ACCESS_TOKEN]
            self.refresh_token = response.json()[REFRESH_TOKEN]
            print_time(arg="The initial access token has been obtained successfully, as well as the refresh token.")
        else:
            print_time(arg=Fore.RED + f"ERROR: It was not possible to get the access token. Error code: {response.status_code}." + Fore.RESET)

        return None
    
    def refresh_access_token(self, refresh_token):
        """
        Get a refreshed access token when the previous current access token has expired.
        Refresh token is obtained as well even it does not change, theoretically.
        """

        print_time(arg="Trying to refresh the access token, please wait.")

        payload = f"grant_type={self.grant_type_refresh}&client_secret={self.client_secret}&client_id={self.client_id}&" + \
                  f"refresh_token={refresh_token}"

        headers = {"Content-type": "application/x-www-form-urlencoded"}

        response = requests.post(self.url_token, data=payload, headers=headers)

        if response.status_code == 200:
            self.access_token = response.json()[ACCESS_TOKEN]
            self.refresh_token = response.json()[REFRESH_TOKEN]
            print_time(arg="The access token has been refreshed successfully, as well as the refresh token.")
        else:
            print_time(arg=Fore.RED + f"ERROR: It was not possible to refresh the token. Error code: {response.status_code}." + Fore.RED)

        return None

    def get_station_data(self, access_token):
        """
        Send the request to get the weather data from the API.
        """

        print_time(arg="Getting the station data from the API, please wait.")

        headers = {'Authorization': 'Bearer ' + f'{access_token}'}
        response = requests.get(url=self.url_api_stations, headers=headers)

        return response

    def get_measure_data(self, access_token, module_id, measure):

        module_name = module_id.replace(COLON_MAPPER, ":")
        print_time(arg=f"Getting the measure {measure} data of the module {module_name} from the API, please wait.")

        # take the start and stop times
        now = int(time.time())
        date_begin = now - 86400
        date_end = now
        
        headers = {'Authorization': 'Bearer ' + f'{access_token}', 'accept': 'application/json'}
        url = self.url_api_devices +f"device_id={self.id_station}&module_id={module_id}&scale={DATA_SCALE}&type={measure}&" + \
                                    f"date_begin={date_begin}&date_end={date_end}&optimize=false&real_time=false"

        response = requests.get(url, headers=headers)

        return response