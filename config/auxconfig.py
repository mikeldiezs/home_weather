import configparser

# initialization config file path
CONFIG_FILE = "config/config.ini"

# database config
TAG_DEVICE = "_id"
TAG_HOME = "home_id"
HOME_ID = "61d75bd04db1a77ca71a4bf4"
TIME_SCALE = 1000000000

######################################### NETATMO API #########################################
# api labels
ACCESS_TOKEN = "access_token"
REFRESH_TOKEN = "refresh_token"

# devices names
STATION_DEVICE = 'Station'
OUTDOOR_DEVICE = "Outdoor"
PLUVIOMETER_DEVICE = "Pluviometer"
ANEMOMETER_DEVICE = "Anemometer"
COLON_MAPPER = "%3A"

# json filters
MODULE_NAME = 'module_name'
JSON_BODY = "body"
JSON_DEVICES = "devices"
JSON_MODULES = "modules"
TIME_KEY = "time_utc"
DASHBOARD_KEY = "dashboard_data"

# data scale
DATA_SCALE = "5min"

# devices relation
config = configparser.ConfigParser()
config.read(CONFIG_FILE)
DEVICES_ID = {STATION_DEVICE: config['netatmo_devices']['id_station'],
              OUTDOOR_DEVICE: config['netatmo_devices']['id_outdoor'],
              PLUVIOMETER_DEVICE: config['netatmo_devices']['id_pluviometer'],
              ANEMOMETER_DEVICE: config['netatmo_devices']['id_anemometer'],
              }

# json stations devices data keys
JSON_DEVICES_KEYS = {STATION_DEVICE: {'wifi_status': 'wifi_status',
                                      'reachable': 'reachable',
                                      'co2_calibrating': 'co2_calibrating',
                                      'place': {'altitude': 'altitude',
                                                'location': ['longitude', 'latitude']
                                               },
                                      'dashboard_data': {'time_utc': 'time_utc',
                                                         'Temperature': 'temperature',
                                                         'CO2': 'co2',
                                                         'Humidity': 'humidity',
                                                         'Noise': 'noise',
                                                         'Pressure': 'pressure',
                                                         'AbsolutePressure': 'absolute_pressure'
                                                        },
                                     },
                     OUTDOOR_DEVICE: {'battery_percent': 'battery_percent',
                                      'reachable': 'reachable',
                                      'rf_status': 'rf_status',
                                      'battery_vp': 'battery_vp',
                                      'dashboard_data': {'time_utc': 'time_utc',
                                                         'Temperature': 'temperature',
                                                         'Humidity': 'humidity'},
                                     },
                     PLUVIOMETER_DEVICE: {'battery_percent': 'battery_percent',
                                          'reachable': 'reachable',
                                          'rf_status': 'rf_status',
                                          'battery_vp': 'battery_vp',
                                          'dashboard_data': {'time_utc': 'time_utc',
                                                             'Rain': 'rain',
                                                             'sum_rain_1': 'sum_rain_1',
                                                             'sum_rain_24': 'sum_rain_24'
                                                            },
                                        },
                     ANEMOMETER_DEVICE: {'battery_percent': 'battery_percent',
                                         'reachable': 'reachable',
                                         'rf_status': 'rf_status',
                                         'battery_vp': 'battery_vp',
                                         'dashboard_data': {'time_utc': 'time_utc',
                                                            'WindStrength': 'windspeed',
                                                            'WindAngle': 'windangle',
                                                            'GustStrength': 'gustspeed',
                                                            'GustAngle': 'gustangle',
                                                            },
                                        }
                     }

# json devices measures data keys
JSON_MEASURES_KEYS = {STATION_DEVICE: {'temperature': 'temperature',
                                       'co2': 'co2',
                                       'humidity': 'humidity',
                                       'noise': 'noise',
                                       'pressure': 'pressure',                
                                      },
                     OUTDOOR_DEVICE: {'temperature': 'temperature',
                                      'humidity': 'humidity',
                                     },
                     PLUVIOMETER_DEVICE: {'rain': 'rain',
                                          'sum_rain': 'sum_rain_1',               
                                         },
                     ANEMOMETER_DEVICE: {'windstrength': 'windspeed',
                                         'windangle': 'windangle',
                                         'guststrength': 'gustspeed',
                                         'gustangle': 'gustangle',
                                        },
                    }