import datetime, time

def print_time(arg):
    ts = datetime.datetime.now().timestamp()
    ts_converted = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(ts))
    print(f'{ts_converted}: ' + arg)