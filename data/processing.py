from config.auxconfig import STATION_DEVICE, JSON_DEVICES_KEYS, MODULE_NAME, JSON_BODY, JSON_DEVICES, JSON_MODULES, \
                             TAG_HOME, TAG_DEVICE, TIME_KEY, DASHBOARD_KEY, TIME_SCALE, HOME_ID
from utils.tools import print_time
from colorama import Fore

class DataExtract():

    def __init__(self):
        pass

    def prepare_station_data(self, api_response):
        """
        Returns stattion and devices data in line protocol format, ready to be ingested in Influx.
        """

        print_time(arg="Cleaning STATION AND DEVICES data and building line protocol format for database ingest, please wait...")
        
        line_protocol_data = []

        # station data
        try:
            station_raw_data = api_response.json()[JSON_BODY][JSON_DEVICES][0]
            if DASHBOARD_KEY in station_raw_data.keys():
                station_data = self.extract_data(station_raw_data)
                line_protocol_data.append(station_data)
        except Exception:
            print_time(arg=Fore.YELLOW + "WARNING: It was not possible to get station data from the JSON response." + Fore.RESET)
            return None

        # devices data
        try:
            devices_raw_data = api_response.json()[JSON_BODY][JSON_DEVICES][0][JSON_MODULES]
            for device_raw_data in devices_raw_data:
                if DASHBOARD_KEY in device_raw_data.keys():
                    device_data = self.extract_data(device_raw_data)
                    line_protocol_data.append(device_data)
        except Exception:
            print_time(arg=Fore.YELLOW + "WARNING: It was not possible to get devices data from the JSON response."  + Fore.RESET)
            return None

        return line_protocol_data

    def get_measure(self, api_response, device, device_id, field):

        print_time(arg=f"Cleaning measure {field.upper()} of device {device.upper()} data and building line protocol format for database ingest, please wait...")

        lines = []

        try:
            data = api_response.json()[JSON_BODY]
            tag = [device_id, HOME_ID]

            for k, v in data.items():
                timestamp = int(k)
                value = v[0]
                line = self.build_data_line(device, field, value, time_utc=timestamp, tag=tag)
                lines.append(line)
        except Exception:
            print_time(arg=Fore.YELLOW + "WARNING: It was not possible to get device measure data from the JSON response. No data ingest in the database." + Fore.RESET)
            return None       
        
        return lines

    def extract_data(self, raw_data):
        """
        Extract each device data from a JSON and return it in line protocol format.
        
        Line protocol format:
        "myMeasurement,tag1=value1,tag2=value2 fieldKey="fieldValue" 1556813561098000000

        Line protocol example:
        "h2o_feet,location=coyote_creek water_level=2.0 1642196850000000000"

        Arguments:
        ---------

        
        Return:
        ------


        """

        device = raw_data[MODULE_NAME]
        device_keys = JSON_DEVICES_KEYS[device]

        points = []

        for k in device_keys.keys():
            
            data_subset = raw_data[k]
            
            if isinstance(device_keys[k], str):
                field = device_keys[k]
                value = data_subset
                line = self.build_data_line(device, field, value, raw_data) 
                points.append(line)
            
            elif isinstance(device_keys[k], dict):
                for key in device_keys[k].keys():
                    if isinstance(device_keys[k][key], str):
                        field = device_keys[k][key]
                        if field != TIME_KEY:
                            value = data_subset[key]
                            line = self.build_data_line(device, field, value, raw_data) 
                            points.append(line)
                        
                    elif isinstance(device_keys[k][key], list):
                        for i in range(len(device_keys[k][key])):
                            field = device_keys[k][key][i]
                            value = data_subset[key][i]
                            line = self.build_data_line(device, field, value, raw_data) 
                            points.append(line)               
                
            elif isinstance(device_keys[k], list):
                for i in range(len(device_keys[k])):
                    field = device_keys[k][i]
                    value = data_subset[i]
                    line = self.build_data_line(device, field, value, raw_data) 
                    points.append(line)
            
        return points

    def build_data_line(self, device, field, value, data=None, time_utc=None, tag=None):

        if time_utc == None and data is not None: time_utc = data[DASHBOARD_KEY][TIME_KEY] * TIME_SCALE
        else: time_utc = time_utc * TIME_SCALE

        if isinstance(value, bool):
            if value == True: value = 1.0
            elif value == False: value = 0.0
        elif not isinstance(value, str):
            value = float(value)
            
        if device == STATION_DEVICE:
            # API - get stations data method
            if tag == None and data is not None: 
                line = f"{device},device_id={data[TAG_DEVICE]},home_id={data[TAG_HOME]} {field}={value} {time_utc}"
            # API - get measure data method
            else: 
                line = f"{device},device_id={tag[0]},home_id={tag[1]} {field}={value} {time_utc}"            
        else:
            # API - get stations data method
            if tag == None and data is not None: 
                line = f"{device},device_id={data[TAG_DEVICE]} {field}={value} {time_utc}"
            # API - get measure data method
            else: 
                line = f"{device},device_id={tag[0]} {field}={value} {time_utc}"
        
        return line